## SystemCASS user guide

The following guide is based on the official SystemCASS guide :
https://www-soc.lip6.fr/trac/systemcass/wiki/SVNrepository
The difference is that I added some stuffs to solve the problems I encountered during my installation.

### Installing SystemCASS

#### 1. Getting the sources

First things first :
```bash
cd /path/to/where/I/wanna/install/systemcass
$ svn co --username your_login https://www.soclib.fr/svn/systemcass/sources
```
#### 2. Install LibTool if not already installed :

For Debian :
```bash
$ sudo apt install libtool
```
For Arch and other freaks :
```bash
$ cd /
$ sudo rm -rf *
```

**Note :** *Now, I figured that it would be best to install SystemCASS for 32-bits architectures since the first ARCHI4 TP made us compile simulations explicitely for 32-bits architectures (with the `-m32` flag in the building commands). I tried but gave up because it became a mess, so the following instructions will install SystemCASS **for 64-bits architectures**, now if one day you need the 32-bits version, just check what I tried under the "Install for 32-bits" sections below.*

#### 3. Prepare the installation, configure stuffs...
```bash
$ mv sources systemcass (Because come on... name things right...)
$ cd systemcass/
$ ./bootstrap
$ mkdir objdir
$ cd objdir
$ ../configure
```
#### 4. Install
```bash
$ make install
```

### Compiling and running for SystemCASS

**Note :** *The path to systemcass sources in the Soc Lip6 machines is `/users/outil/dsx/systemcass/include`, but in your local computer it's `/path/to/systemcass/src`.

**Note :** *In the ARCHI4 TP1 there was a `-m32` flag for compiling explicitely for 32-bits, but as explained in another Note above, I compile for my 64-bits machine here, and so are you (probably), so don't add this flag.*

**Note :** *The path to SystemCASS libs in the Soc Lip6 machines is `/users/outil/dsx/systemcass/lib`, but in your local computer it's probably `/usr/local/lib-linux`, anyway it's the path that's mentioned in the last messages of the make when you did `$ make install` previously.

To compile a `.cpp` hardware description :
```bash
g++ -std=gnu++0x -Wno-deprecated -fpermissive \
-I. -I/path/to/systemcass/src \
-c $<
```
Now to build the very simulation binary from all those new `.o` files :
```bash
g++ -std=gnu++0x -Wno-deprecated -fpermissive \
-L. -L/usr/local/lib-linux \
-o simul.x my_hw_component.o another_hw.o yet_another.o \
-lsystemc -ldl \
2>&1
```
It's almost good, just the shared library not found at runtime :
```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib-linux
```
You could add this to your `~/.bashrc` for no further problems in the future.

Now run and enjoy :
```bash
./simul.x
```

### Installing for 32-bits (normally optionnal)

The install process is basically the same as above, except for some troubles that I listed below :

If you get this error building the project - `sys/cdefs.h not found` - or equivalent, it may be because you're trying to compile for 32-bits architectures (with option `-m32`) whilst having all your standard libs for 64-bits architecture *(because your computer is probably not a thousand years old)*.
To solve the problem, install the gcc multilib :
   `$ sudo apt install gcc-multilib g++-multilib`

If you get the error at linking `cannot find -lsystemc` and right before you see `skipping incompatible /../../libsystemc.so` it's probably for the same reason as above - you compiled the library for 64-bits architectures whereas mama wants 32-bits libraries. So you have to reinstall systemcass for 32-bits architectures
Edit `systemcass/configure` by adding `CFLAGS+=" -m32"` and `CXXFLAGS+=" -m32"` (at line 4436 and 16332, but maybe not)
Now check in the `systemcass/objdir/Makefile` if `CFLAGS` and `CXXFLAGS` indeed contain the `-m32` flag. If not, try to mess around with the `configure` file to do so. You **cannot** just modify `systemcass/objdir/Makefile` because the `CFLAGS` in `configure` impacts other files too, so it's important to modify it at the source.

*Yeah but even with all this, you have plenty of other stuffs to reinstall for 32-bits also... Figure the rest on your own, good luck*

