
## Rust notes

Difference `println` != `println!`, the former calls a *function* whereas the latter calls a *macro*.

The big three cargo commands :
`cargo new`
`cargo build`
`cargo run`
It's as simple as that. ;)
Also, if you write code and wanna periodically just check if it still compiles, use :
`cargo check`
It's much faster than a full build because it doesn't produce the executable.	

There are two executable profiles - *debug* and *release*
`cargo build` compiles for debug, it's faster than release but doesn't have the optimizations,
*it is preferred when debugging, i.e. when compiling often for testing*
`cargo build --release` compiles for release, it includes optimizations for running faster but the building process is slower. It's intended to be used for the final program given to the user.
